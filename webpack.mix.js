const mix = require('laravel-mix');
let LiveReloadPlugin = require('webpack-livereload-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();

mix.js('resources/js/index.js', 'public/js')
    .js('resources/js/map.js', 'public/js')
    .js('resources/js/audio.js', 'public/js')

    .js('resources/js/auth/login.js', 'public/js/auth')
    .js('resources/js/auth/password/email.js', 'public/js/auth/password')
    .js('resources/js/auth/guard/guard.js', 'public/js/auth/guard')

    .js('resources/js/account/inbox/index.js', 'public/js/account/inbox')

    .js('resources/js/prospect/index.js', 'public/js/prospect')
    .js('resources/js/prospect/create.js', 'public/js/prospect')
    .js('resources/js/prospect/map.js', 'public/js/prospect')
    .js('resources/js/prospect/show.js', 'public/js/prospect')

mix.webpackConfig({
    resolve: {
        alias: {
            'morris.js': 'morris.js/morris.js',
            'jquery-ui': 'jquery-ui',
        },
    },
    plugins: [
        new LiveReloadPlugin()
    ]
});
