<?php

namespace App\Console\Commands;

use App\Notifications\Auth\CreateCommandUserNotification;
use App\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Création d'un utilisateur";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $user = User::create([
                "name" => $this->argument('name'),
                "email" => $this->argument('email'),
                "password" => bcrypt($this->argument('password'))
            ]);
            $this->info("Inscription de l'utilisateur terminer");
            $this->info("Envoie de l'email au nouvelle utilisateur");
            try {
                $user->notify(new CreateCommandUserNotification($user, $this->argument('password')));
                $this->info("Création de l'utilisateur terminer");
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
        return null;
    }
}
