<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 13/02/2020
 * Time: 18:29
 */

namespace App\Helpers;


class Prospect
{

    public static function stateRdv($state)
    {
        switch ($state) {
            case 0: return 'info';
            case 1: return 'warning';
            case 2: return 'danger';
            default: return 'info';
        }
    }

    public static function getBadgeProspect($status)
    {
        switch ($status) {
            case 0:
                return '<span class="kt-badge kt-badge--inline kt-badge--danger">Nouveau</span>';
            case 1:
                return '<span class="kt-badge kt-badge--inline kt-badge--info">En attente de rendez-vous</span>';
            case 2:
                return '<span class="kt-badge kt-badge--inline kt-badge--warning">Réponse en attente</span>';
            case 3:
                return '<span class="kt-badge kt-badge--inline kt-badge--success">Finaliser</span>';
            default:
                return null;
        }
    }

    public static function updateBlockUserApi($status)
    {
        switch ($status) {
            case 0:
                return '<span class="kt-badge kt-badge--inline kt-badge--danger">Nouveau</span>';
            case 1:
                return '<span class="kt-badge kt-badge--inline kt-badge--info">En attente de rendez-vous</span>';
            case 2:
                return '<span class="kt-badge kt-badge--inline kt-badge--warning">Réponse en attente</span>';
            case 3:
                return '<span class="kt-badge kt-badge--inline kt-badge--success">Finaliser</span>';
            default:
                return null;
        }
    }

    public static function getSourceString($source)
    {
        switch ($source) {
            case 0:
                return 'Visite';
            case 1:
                return 'Téléphone';
            case 2:
                return 'Internet';
            default:
                return 'inconnue';
        }
    }
}
