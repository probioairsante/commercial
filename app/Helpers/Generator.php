<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sylth
 * Date: 12/02/2020
 * Time: 13:55
 */

namespace App\Helpers;


use Illuminate\Support\Str;

class Generator
{
    public static function pluralizer(string $word, $count)
    {
        if($count >= 2){
            return $word.'s';
        }else{
            return $word;
        }
    }

    public static function firsLetter(string $word)
    {
        return Str::limit($word, 1, null);
    }

    /**
     * Permet de définir si une route est active ou non
     * TODO: Pensez à le parametrer à chaque initialisation de projet
     *
     * @param array ...$routes
     * @return string|null
     */
    public static function currentRoute(...$routes)
    {
        foreach ($routes as $route){
            if(request()->url() == $route){
                return "kt-menu__item--here";
            }else{
                return null;
            }
        }
        return null;
    }
}
