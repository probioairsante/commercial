<?php
namespace App\Repository\Prospect;



use App\Model\Prospect\Prospect;

class ProspectRepository
{
    /**
     * @var Prospect
     */
    private $prospect;

    /**
     * ProspectRepository constructor.
     * @param Prospect $prospect
     */

    public function __construct(Prospect $prospect)
    {
        $this->prospect = $prospect;
    }

    public function listForUser()
    {
        return $this->prospect->newQuery()
            ->where('user_id', auth()->user()->id)
            ->orderBy('name', 'asc')
            ->get();
    }

    public function countProspect()
    {
        return $this->prospect->newQuery()
            ->where('user_id', auth()->user()->id)
            ->count();
    }

    public function listForUserWithStatus($status)
    {
        return $this->prospect->newQuery()
            ->where('user_id', auth()->user()->id)
            ->where('status', $status)
            ->orderBy('name', 'asc')
            ->get();
    }

    public function create($societe, $name, $adresse, $code_postal, $ville, $email, $tel_fixe, $tel_portable, $source, $status)
    {
        return $this->prospect->newQuery()
            ->create([
                "user_id" => auth()->user()->id,
                "societe" => $societe,
                "name" => $name,
                "adresse" => $adresse,
                "code_postal" => $code_postal,
                "ville" => $ville,
                "email" => $email,
                "tel_fixe" => $tel_fixe,
                "tel_portable" => $tel_portable,
                "source" => $source,
                "status" => $status
            ]);
    }

    public function get($prospect_id)
    {
        return $this->prospect->newQuery()
            ->find($prospect_id);
    }

    public function changeStatus($prospect_id, $status)
    {
        $this->prospect->newQuery()
            ->find($prospect_id)
            ->update([
                "status" => $status
            ]);

        return $this->prospect->newQuery()
            ->find($prospect_id);

    }


}

