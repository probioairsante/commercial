<?php
namespace App\Repository\Inbox;

use App\Model\Inbox\Inbox;

class InboxRepository
{
    /**
     * @var Inbox
     */
    private $inbox;

    /**
     * InboxRepository constructor.
     * @param Inbox $inbox
     */

    public function __construct(Inbox $inbox)
    {
        $this->inbox = $inbox;
    }

    public function listFormBox($type)
    {
        return $this->inbox->newQuery()
            ->where('user_id_to', auth()->user()->id)
            ->where('type', $type)
            ->get();
    }

}

