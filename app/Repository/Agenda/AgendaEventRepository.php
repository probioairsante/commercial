<?php
namespace App\Repository\Agenda;

use App\Model\Agenda\AgendaEvent;
use Illuminate\Support\Carbon;

class AgendaEventRepository
{
    /**
     * @var AgendaEvent
     */
    private $agendaEvent;

    /**
     * AgendaEventRepository constructor.
     * @param AgendaEvent $agendaEvent
     */

    public function __construct(AgendaEvent $agendaEvent)
    {
        $this->agendaEvent = $agendaEvent;
    }

    public function listForProspect($prospect_id)
    {
        return $this->agendaEvent->newQuery()
            ->where('prospect_id', $prospect_id)
            ->orderBy('start', 'asc')
            ->get();
    }

    public function get($rdv_id)
    {
        return $this->agendaEvent->newQuery()
            ->find($rdv_id);
    }

    public function createFromProspect($id, $prospect_id, $title, $start, $end, $description)
    {
        return $this->agendaEvent->newQuery()
            ->create([
                "user_id"   => $id,
                "prospect_id" => $prospect_id,
                "title" => $title,
                "start" => Carbon::createFromTimestamp(strtotime($start)),
                "end" => Carbon::createFromTimestamp(strtotime($end)),
                "description" => $description
            ]);
    }

    public function getLatest()
    {
        return $this->agendaEvent->newQuery()
            ->where('user_id', auth()->user()->id)
            ->where('start', '>=', now())
            ->orderBy('start', 'asc')
            ->get()
            ->first();
    }

    public function list()
    {
        return $this->agendaEvent->newQuery()
            ->where('user_id', auth()->user()->id)
            ->where('start', '>=', now())
            ->orderBy('start', 'asc')
            ->limit(10)
            ->get();
    }

}

