<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Repository\Inbox\InboxRepository;
use Illuminate\Http\Request;

class InboxController extends Controller
{
    /**
     * @var InboxRepository
     */
    private $inboxRepository;

    /**
     * InboxController constructor.
     * @param InboxRepository $inboxRepository
     */
    public function __construct(InboxRepository $inboxRepository)
    {
        $this->inboxRepository = $inboxRepository;
    }

    public function index()
    {
        return view("account.inbox.index");
    }
}
