<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
    }

    public function locked(Request $request)
    {
        return view("auth.guard.guard");
    }

    public function postGuard(Request $request)
    {
        if (\request()->ajax()) {
            $user = auth()->user();
            //dd($user);
            if ($user->verifCode == $request->get('code')) {
                try {
                    User::find($user->id)
                        ->update([
                            "lastIp" => $request->ip()
                        ]);

                    return response()->json([
                        "linker" => '/'
                    ]);
                } catch (\Exception $exception) {
                    return response()->json([
                        "error" => $exception->getMessage()
                    ], 500);
                }
            } else {
                return response()->json([
                    "error" => "Les codes ne correspondent pas !"
                ], 422);
            }
        }
    }
}
