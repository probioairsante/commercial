<?php

namespace App\Http\Controllers\Prospect;

use App\Helpers\Generator;
use App\Helpers\Prospect;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Notifications\Prospect\NewSchedule;
use App\Repository\Agenda\AgendaEventRepository;
use App\Repository\Prospect\ProspectRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;

class ProspectApiController extends BaseController
{
    /**
     * @var ProspectRepository
     */
    private $prospectRepository;
    /**
     * @var AgendaEventRepository
     */
    private $agendaEventRepository;

    /**
     * ProspectApiController constructor.
     * @param ProspectRepository $prospectRepository
     * @param AgendaEventRepository $agendaEventRepository
     */
    public function __construct(ProspectRepository $prospectRepository, AgendaEventRepository $agendaEventRepository)
    {
        $this->prospectRepository = $prospectRepository;
        $this->agendaEventRepository = $agendaEventRepository;
    }

    public function list()
    {
        $datas = $this->prospectRepository->listForUser();
        ob_start();
        ?>
        <?php foreach ($datas as $data): ?>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">

                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown">
                            <i class="flaticon-more-1 kt-font-brand"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="kt-nav">
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                        <span class="kt-nav__link-text">Reports</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Widget -->
                    <div class="kt-widget kt-widget--user-profile-2">
                        <div class="kt-widget__head">
                            <div class="kt-widget__media">
                                <?php if (Gravatar::exists($data->email)): ?>
                                    <img class="kt-widget__img kt-hidden-" src="<?= Gravatar::src($data->email); ?>"
                                         alt="image">
                                <?php else: ?>
                                    <div
                                        class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                        <?= Generator::firsLetter($data->name); ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="kt-widget__info">
                                <a href="#" class="kt-widget__username">
                                    <?= $data->name; ?>
                                </a>

                                <span class="kt-widget__desc">
                                        <?= $data->email; ?><br>
                                    <?= Prospect::getBadgeProspect($data->status); ?>
                                    </span>
                            </div>
                        </div>

                        <div class="kt-widget__body">
                            <div class="kt-widget__section">
                                <?= $data->description; ?>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label">Email:</span>
                                    <a href="#" class="kt-widget__data"><?= $data->email; ?></a>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label">Téléphone:</span>
                                    <a href="#" class="kt-widget__data"><?= $data->tel_fixe; ?></a>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label">Location:</span>
                                    <span class="kt-widget__data">
                                        <?= $data->adresse; ?><br>
                                        <?= $data->code_postal; ?> <?= $data->ville; ?>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="kt-widget__footer">
                            <a href="<?= route('Prospect.show', $data->id); ?>" id="btnViewProfil"
                               class="btn btn-label-primary btn-lg btn-upper">Voir le Profil</a>
                        </div>
                    </div>
                    <!--end::Widget -->
                </div>
            </div>
        </div>
    <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "OK");
    }

    public function postList(Request $request)
    {
        $datas = $this->prospectRepository->listForUserWithStatus($request->status);
        ob_start();
        ?>
        <?php foreach ($datas as $data): ?>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">

                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown">
                            <i class="flaticon-more-1 kt-font-brand"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="kt-nav">
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                        <span class="kt-nav__link-text">Reports</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Widget -->
                    <div class="kt-widget kt-widget--user-profile-2">
                        <div class="kt-widget__head">
                            <div class="kt-widget__media">
                                <?php if (Gravatar::exists($data->email)): ?>
                                    <img class="kt-widget__img kt-hidden-" src="<?= Gravatar::src($data->email); ?>"
                                         alt="image">
                                <?php else: ?>
                                    <div
                                        class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden-">
                                        <?= Generator::firsLetter($data->name); ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="kt-widget__info">
                                <a href="#" class="kt-widget__username">
                                    <?= $data->name; ?>
                                </a>

                                <span class="kt-widget__desc">
                                        <?= $data->email; ?><br>
                                    <?= Prospect::getBadgeProspect($data->status); ?>
                                    </span>
                            </div>
                        </div>

                        <div class="kt-widget__body">
                            <div class="kt-widget__section">
                                <?= $data->description; ?>
                            </div>

                            <div class="kt-widget__item">
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label">Email:</span>
                                    <a href="#" class="kt-widget__data"><?= $data->email; ?></a>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label">Téléphone:</span>
                                    <a href="#" class="kt-widget__data"><?= $data->tel_fixe; ?></a>
                                </div>
                                <div class="kt-widget__contact">
                                    <span class="kt-widget__label">Location:</span>
                                    <span class="kt-widget__data">
                                        <?= $data->adresse; ?><br>
                                        <?= $data->code_postal; ?> <?= $data->ville; ?>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="kt-widget__footer">
                            <a href="<?= route('Prospect.show', $data->id); ?>" id="btnViewProfil"
                               class="btn btn-label-primary btn-lg btn-upper">Voir le Profil</a>
                        </div>
                    </div>
                    <!--end::Widget -->
                </div>
            </div>
        </div>
    <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "OK");
    }

    public function count()
    {
        $data = $this->prospectRepository->countProspect();

        return $this->sendResponse($data . ' ' . Generator::pluralizer('Prospect', $data), "Nombre de Prospect");
    }

    public function loadRdv($prospect_id)
    {
        $datas = $this->agendaEventRepository->listForProspect($prospect_id);
        ob_start();
        ?>
        <div class="kt-timeline-v3">
            <div class="kt-timeline-v3__items">
                <?php foreach ($datas as $data): ?>
                    <div class="kt-timeline-v3__item kt-timeline-v3__item--<?= Prospect::stateRdv($data->className); ?>" data-rdv="<?= $data->id; ?>">
                        <span class="kt-timeline-v3__item-time"><?= $data->start->format("d/m") ?></span>
                        <div class="kt-timeline-v3__item-desc">
								<span class="kt-timeline-v3__item-text">
								<?= $data->title ?>
                                    <span class="h6"><?= $data->description ?></span>
								</span><br>
                            <span class="kt-timeline-v3__item-user-name">
								<a href="#" class="kt-link kt-link--dark kt-timeline-v3__itek-link">
								<?= $data->start->format("H:i") ?> - <?= $data->end->format("H:i") ?>
								</a>
								</span>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "Rendez-vous");
    }

    public function getRdv($prospect_id, $rdv_id)
    {
        $data = $this->agendaEventRepository->get($rdv_id);

    }

    public function createRdv(Request $request, $prospect_id)
    {
        $validator = \Validator::make($request->all(), [
            'title' => "required|min:5",
            'start' => "required",
            'end' => "required"
        ]);

        if ($validator->fails()) {
            return $this->sendError("Erreur de validation", ["errors" => $validator->errors()->all()], 203);
        }

        try {
            $prospect = $this->prospectRepository->get($prospect_id);
            $rdv = $this->agendaEventRepository->createFromProspect(
                auth()->user()->id,
                $prospect_id,
                $request->title,
                $request->start,
                $request->end,
                $request->description
            );
            ob_start();
            ?>
            <div class="kt-timeline-v3__item kt-timeline-v3__item--<?= Prospect::stateRdv($rdv->className); ?>" data-rdv="<?= $rdv->id; ?>">
                <span class="kt-timeline-v3__item-time"><?= $rdv->start->format("d/m") ?></span>
                <div class="kt-timeline-v3__item-desc">
								<span class="kt-timeline-v3__item-text">
								<?= $rdv->title ?>
                                    <span class="h6"><?= $rdv->description ?></span>
								</span><br>
                    <span class="kt-timeline-v3__item-user-name">
								<a href="#" class="kt-link kt-link--dark kt-timeline-v3__itek-link">
								<?= $rdv->start->format("H:i") ?> - <?= $rdv->end->format("H:i") ?>
								</a>
								</span>
                </div>
            </div>
            <?php
            $content = ob_get_clean();

            auth()->user()->notify(new NewSchedule($prospect, $rdv));

            return $this->sendResponse(['content' => $content], "Create RDV");
        } catch (\Exception $exception) {
            return $this->sendError("Erreur Serveur", ["errors" => $exception->getMessage()]);
        }
    }

    public function previousRdv()
    {
        $data = $this->agendaEventRepository->getLatest();
        ob_start();
        ?>
        <div class="alert alert-primary" role="alert">
            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
            <div class="alert-text">Prochain rendez-vous dans <strong><?= $data->start->diffForHumans() ?></strong>
            </div>
        </div>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "Prochain Rendez-vous");
    }

    public function rdv()
    {
        $datas = $this->agendaEventRepository->list();
        ob_start();
        ?>
        <div class="kt-timeline-v3">
            <div class="kt-timeline-v3__items">
                <?php foreach ($datas as $data): ?>
                    <div class="kt-timeline-v3__item kt-timeline-v3__item--<?= Prospect::stateRdv($data->className); ?>" data-rdv="<?= $data->id; ?>">
                        <span class="kt-timeline-v3__item-time"><?= $data->start->format("d/m") ?></span>
                        <div class="kt-timeline-v3__item-desc">
								<span class="kt-timeline-v3__item-text">
                                    <strong><?= $data->prospect->name; ?></strong><br>
								    <?= $data->title ?>
                                    <span class="h6"><?= $data->description ?></span>
								</span><br>
                            <span class="kt-timeline-v3__item-user-name">
								<a href="#" class="kt-link kt-link--dark kt-timeline-v3__itek-link">
								<?= $data->start->format("H:i") ?> - <?= $data->end->format("H:i") ?>
								</a>
								</span>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse(['content' => $content, 'info' => $datas], "RDV");
    }
}
