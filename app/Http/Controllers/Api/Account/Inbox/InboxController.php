<?php

namespace App\Http\Controllers\Api\Account\Inbox;

use App\Helpers\Generator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Repository\Inbox\InboxRepository;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;

class InboxController extends BaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var InboxRepository
     */
    private $inboxRepository;

    /**
     * InboxController constructor.
     * @param UserRepository $userRepository
     * @param InboxRepository $inboxRepository
     */
    public function __construct(UserRepository $userRepository, InboxRepository $inboxRepository)
    {
        $this->userRepository = $userRepository;
        $this->inboxRepository = $inboxRepository;
    }

    public function getList($type_box)
    {
        $type = $this->typeBox($type_box);
        $datas = $this->inboxRepository->listFormBox($type);
        ob_start();
        ?>
        <?php foreach ($datas as $data): ?>
        <div class="kt-inbox__item kt-inbox__item--unread" data-id="<?= $data->id; ?>" data-type="<?= $type_box; ?>">
            <div class="kt-inbox__info">
                <div class="kt-inbox__actions">
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                        <input type="checkbox">
                        <span></span>
                    </label>
                </div>
                <div class="kt-inbox__sender" data-toggle="view">
															<span class="kt-media kt-media--sm kt-media--danger" style="background-image: url('assets/media/users/100_13.jpg')">
																<span></span>
															</span>
                    <a href="#" class="kt-inbox__author"><?= $data->userFrom->name; ?></a>
                </div>
            </div>
            <div class="kt-inbox__details" data-toggle="view">
                <div class="kt-inbox__message">
                    <span class="kt-inbox__subject"><?= $data->subject; ?> - </span>
                    <span class="kt-inbox__summary">Thank you for ordering UFC 240 Holloway vs Edgar Alternate camera angles...</span>
                </div>
            </div>
            <div class="kt-inbox__datetime" data-toggle="view">
                <?= $data->updated_at->diffForHumans(); ?>
            </div>
        </div>
        <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "OK");
    }


}
