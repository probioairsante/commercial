<?php

namespace App\Http\Controllers\Api\Account;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountController extends BaseController
{
    public function __construct()
    {
    }

    public function readNotification($notification_id)
    {
        try {
            $data = DB::table('notifications')->where('id', $notification_id)->update(["read_at" => now()]);
            return $this->sendResponse($data, "Notification mis à jour");
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(), "Erreur", 422);
        }
    }

    public function postGuard(Request $request)
    {

    }
}
