<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;

class CoreController extends BaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function verifyMail(Request $request)
    {
        $data = $this->userRepository->countEmail($request->email);

        if($data > 0) {
            return $this->sendResponse("OK", "L'email Existe", "success");
        }else{
            return $this->sendError("false", "L'email n'existe pas", 422);
        }
    }
}
