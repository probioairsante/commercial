<?php

namespace App\Model\Prospect;

use App\Model\Agenda\AgendaEvent;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Prospect extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function events()
    {
        return $this->hasMany(AgendaEvent::class);
    }
}
