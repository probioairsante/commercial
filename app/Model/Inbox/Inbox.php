<?php

namespace App\Model\Inbox;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $guarded = [];

    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_id_from');
    }

    public function userTo()
    {
        return $this->belongsTo(User::class, 'user_id_to');
    }

    public function message()
    {
        return $this->hasOne(InboxMessage::class);
    }
}
