<?php

namespace App\Notifications\Prospect;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\FcmNotification;

class NewSchedule extends Notification
{
    use Queueable;
    /**
     * @var
     */
    public $prospect;
    /**
     * @var
     */
    public $event;

    /**
     * Create a new notification instance.
     *
     * @param $prospect
     * @param $event
     */
    public function __construct($prospect, $event)
    {
        //
        $this->prospect = $prospect;
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', FcmChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Nouveau rendez-vous")
            ->markdown('mail.prospect.newSchedule', ["prospect" => $this->prospect, "event" => $this->event]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => "fa fa-calendar",
            "icon_color" => "info",
            "type" => "event",
            "title" => "Nouveau rendez-vous",
            "text" => "Un nouveau rendez-vous avec " . $this->prospect->name . " à été ajouter",
            "date" => now(),
            "link" => route('Prospect.show', $this->prospect->id)
        ];
    }

    public function toFcm($notifiable)
    {
        $fcmNotification = FcmNotification::create()
            ->setTitle('Nouveau Rendez-vous')
            ->setBody('Un nouveau rendez-vous avec '.$this->prospect->name." à été programmer")
            ->setBody("Date: ".$this->event->start->format("d/m/Y à H:i"))
            ->setIcon('/storage/logo_carre.png')
            ->setColor('#00A6E0');

        return FcmMessage::create()
            ->setPriority(FcmMessage::PRIORITY_NORMAL)
            ->setTimeToLive(86400)
            ->setNotification($fcmNotification);
    }
}
