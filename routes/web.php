<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["middleware" => ["auth"]], function (){
    Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);

    Route::group(["prefix" => "account", "namespace" => "Account"], function (){
        Route::get('locked', ["as" => "Account.locked", "uses" => "AccountController@locked"]);
        Route::post('locked', ["as" => "Account.postGuard", "uses" => "AccountController@postGuard"]);

        Route::group(["prefix" => "profil"], function (){
            Route::get('/', ["as" => "Account.Profil.index", "uses" => "ProfilController@index"]);
        });
        Route::group(["prefix" => "inbox"], function (){
            Route::get('/', ["as" => "Account.Inbox.index", "uses" => "InboxController@index"]);

            Route::get('getList/{type_box}', 'InboxApiController@getList');
        });

        Route::group(["prefix" => "schedule"], function (){
            Route::get('/', ["as" => "Account.Schedule.index", "uses" => "ScheduleController@index"]);
        });

        Route::group(["prefix" => "task"], function (){
            Route::get('/', ["as" => "Account.Task.index", "uses" => "TaskController@index"]);
        });
    });

    Route::group(["prefix" => "prospect", "namespace" => "Prospect"], function (){
        Route::get('/', ["as" => "Prospect.index", "uses" => "ProspectController@index"]);
        Route::get('create', ["as" => "Prospect.create", "uses" => "ProspectController@create"]);
        Route::post('create', ["as" => "Prospect.store", "uses" => "ProspectController@store"]);
        Route::get('{prospect_id}', ["as" => "Prospect.show", "uses" => "ProspectController@show"]);
        Route::get('{prospect_id}/vcard', ["as" => "Prospect.vcard", "uses" => "ProspectController@vcard"]);

        Route::group(["prefix" => "api"], function (){
            Route::get('list', 'ProspectApiController@list');
            Route::post('list', 'ProspectApiController@postList');
            Route::get('count', 'ProspectApiController@count');
            Route::get('previousRdv', 'ProspectApiController@previousRdv');
            Route::get('loadRdv', 'ProspectApiController@rdv');

            Route::get('{prospect_id}/loadRdv', 'ProspectApiController@loadRdv');
            Route::get('{prospect_id}/geoloc', 'ProspectApiController@geoloc');
            Route::get('{prospect_id}/rdv/{rdv_id}', 'ProspectApiController@getRdv');

            Route::post('{prospect_id}/rdv', 'ProspectApiController@createRdv');
        });
    });
});

Auth::routes();
Route::get('/logout', ["as" => "logout", "uses" => "Auth\LoginController@logout"]);

Route::get('code', 'TestController@code');
Route::get('mail', 'TestController@mail');
