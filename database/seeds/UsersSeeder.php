<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            "name" => "Mockelyn Maxime",
            "email" => "mmockelyn@gmail.com",
            "password" => bcrypt('1992_Maxime')
        ]);
    }
}
