<?php

use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();

        foreach ($users as $user) {
            foreach ($user->prospects as $prospect) {
                for($i=0; $i < 5; $i++) {
                    $start = now()->addDays(rand(0,15))->addHours(rand(0,23))->addMinutes(rand(0,59));
                    \App\Model\Agenda\AgendaEvent::create([
                        "user_id" => $user->id,
                        "prospect_id" => $prospect->id,
                        "title" => "Rendez-vous",
                        "start" => $start,
                        "end" => $start->addHour(),
                        "description" => null,
                        "className" => rand(0,3)
                    ]);
                }
            }
        }
    }
}
