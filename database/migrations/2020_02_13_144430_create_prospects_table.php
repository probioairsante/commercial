<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('societe');
            $table->string('name');
            $table->string('adresse');
            $table->string('code_postal');
            $table->string('ville');
            $table->string('email');
            $table->string('tel_fixe')->nullable();
            $table->string('tel_portable')->nullable();
            $table->text('description')->nullable();
            $table->integer('source')->comment("0: Visite |1: Téléphone |2: Internet");
            $table->integer('status')->default(0)->comment("0: Nouveau |1: en Attente de rendez-vous |2: En attente de réponse client |3: Finaliser");
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('agenda_events', function (Blueprint $table) {
            $table->foreign('prospect_id')->references('id')->on('prospects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospects');
    }
}
