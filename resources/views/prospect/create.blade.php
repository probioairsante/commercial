@extends("layout.app")

@section("subheader")
    @include("layout.include.subheader", [
        "pages" => [
            [
                "name" => "Prospect",
                "link" => route('home')
            ],
            [
                "name" => "Création d'un prospect",
                "link" => route('home')
            ]
        ],
        "back" => true
    ])
@endsection

@section("content")
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Création d'un nouveau prospect
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form id="formNewProspect" action="{{ route('Prospect.store') }}" class="kt-form" method="POST">
                @csrf
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="societe">Société <span class="required">*</span> </label>
                        <input type="text" class="form-control" name="societe" placeholder="Nom de la société">
                    </div>
                    <div class="col-md-6">
                        <label for="name">Identité <span class="required">*</span> </label>
                        <input type="text" class="form-control" name="name" placeholder="Nom & Prénom du contact">
                    </div>
                </div>
                <div class="form-group">
                    <label for="adresse">Adresse Postal <span class="required">*</span> </label>
                    <input type="text" class="form-control" name="adresse" placeholder="Adresse postal de la société">
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label for="code_postal">Code Postal <span class="required">*</span> </label>
                        <input type="text" id="code_postal" class="form-control" name="code_postal" placeholder="Code postal">
                    </div>
                    <div class="col-md-8">
                        <label for="ville">Ville <span class="required">*</span> </label>
                        <input type="text" class="form-control" name="ville" placeholder="Ville">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Adresse Mail <span class="required">*</span> </label>
                    <input type="email" class="form-control" name="email" placeholder="Email de contact">
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="tel_fixe">Téléphone Fixe </label>
                        <input type="text" id="tel_fixe" class="form-control" name="tel_fixe" placeholder="Numéro de téléphone">
                    </div>
                    <div class="col-md-6">
                        <label for="tel_portable">Téléphone Portable </label>
                        <input type="text" id="tel_portable" class="form-control" name="tel_portable" placeholder="Numéro de téléphone">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Description </label>
                    <textarea name="description" class="form-control" id="description" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="source">Source de contact <span class="required">*</span> </label>
                        <select class="form-control selectpicker" id="source" name="source">
                            <option value="0">Visite</option>
                            <option value="1">Téléphone</option>
                            <option value="2">Internet</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="status">Etat de contact </label>
                        <select class="form-control selectpicker" id="status" name="status">
                            <option value="0">Nouveau contact</option>
                            <option value="1">En attente de rendez-vous</option>
                            <option value="2">En attente de décision client</option>
                            <option value="3">Finaliser</option>
                        </select>
                    </div>
                </div>
                <div class="kt-form__actions kt-form__actions--right">
                    <button type="submit" id="btnSubmitForm" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script src="{{ asset('js/prospect/create.js') }}"></script>
@endsection
