@extends("layout.app")

@section("subheader")
    @include("layout.include.subheader", [
        "pages" => [
            [
                "name" => "Mes Prospects",
                "link" => route('home')
            ],
            [
                "name" => $prospect->name,
                "link" => route('home')
            ]
        ],
        "back" => false
    ])
@endsection

@section("style")
@endsection

@section("content")
    <div class="kt-portlet" id="prospect"
         data-id="{{ $prospect->id }}"
         data-address="{{ $prospect->adresse }}, {{ $prospect->code_postal }} {{ $prospect->ville }}, FR"
         data-societe="{{ $prospect->societe }}"
         data-name="{{ $prospect->name }}"
         data-email="{{ $prospect->email }}"
         data-fixe="{{ $prospect->tel_fixe }}"
         data-portable="{{ $prospect->tel_portable }}">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    @if(\Thomaswelton\LaravelGravatar\Facades\Gravatar::exists($prospect->email))
                        <div class="kt-widget__media">
                            <img src="{{ \Thomaswelton\LaravelGravatar\Facades\Gravatar::src($prospect->email) }}" alt="image">
                        </div>
                    @else
                        <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-bolder kt-font-light kt-hidden-">
                            {{ \App\Helpers\Generator::firsLetter($prospect->name) }}
                        </div>
                    @endif
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <div class="kt-widget__user">
                                <a href="#" class="kt-widget__username">
                                    {{ $prospect->name }}
                                </a>

                                {!! \App\Helpers\Prospect::getBadgeProspect($prospect->status) !!}

                                <div class="dropdown dropdown-inline kt-margin-l-5" data-toggle="kt-tooltip-" title="Change label" data-placement="right">
                                    <a href="#" class="btn btn-clean btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-caret-down"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-md dropdown-menu-fit dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__head">
                                                Changer le status:
                                                <i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Changer le status de ce prospect"></i>
                                            </li>
                                            <li class="kt-nav__separator">
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link link-status" data-toggle="status-change" data-status="0">
                                                    <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--bold">Nouveau</span></span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link link-status" data-toggle="status-change" data-status="1">
                                                    <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-info kt-badge--inline kt-badge--lg kt-badge--bold">En attente de rendez-vous</span></span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link link-status" data-toggle="status-change" data-status="2">
                                                    <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--bold">en attente de résponse</span></span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link link-status" data-toggle="status-change" data-status="3">
                                                    <span class="kt-nav__link-text"><span class="kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--bold">Finalisé</span></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-widget__action">
                                <div class="dropdown dropdown-inline">
                                    <a href="#" class="btn btn-brand btn-sm btn-upper dropdown-toggle" data-toggle="dropdown">
                                        Exporter
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
                                        <!--begin::Nav-->
                                        <ul class="kt-nav">
                                            <li class="kt-nav__head">
                                                Options d'exportation
                                            </li>
                                            <li class="kt-nav__separator"></li>
                                            <li class="kt-nav__item">
                                                <a href="{{ route('Prospect.vcard', $prospect->id) }}" id="btnVcard" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon-layers"></i>
                                                    <span class="kt-nav__link-text">vCard</span>
                                                </a>
                                            </li>

                                        </ul>
                                        <!--end::Nav-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-widget__subhead">
                            <a href="#"><i class="flaticon2-new-email"></i>{{ $prospect->email }}</a>
                            @if(!empty($prospect->tel_fixe))
                                <a href="#"><i class="flaticon2-phone"></i> {{ $prospect->tel_fixe }} </a>
                            @endif
                            @if(!empty($prospect->tel_fixe))
                                <a href="#"><i class="la la-mobile-phone"></i> {{ $prospect->tel_portable }} </a>
                            @endif
                            <a href="#"><i class="flaticon2-website"></i> {{ \App\Helpers\Prospect::getSourceString($prospect->source) }} </a>
                            <a href="#"><i class="flaticon2-placeholder"></i>{{ $prospect->adresse }}, {{ $prospect->code_postal }} {{ $prospect->ville }}, FR</a>
                        </div>

                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                {{ $prospect->description }}
                            </div>
                            <div class="kt-widget__progress">
                                <div class="kt-widget__text">
                                    Etat
                                </div>
                                <div class="progress" style="height: 5px;width: 100%;">
                                    @switch($prospect->status)
                                        @case(0)
                                        <div class="progress-bar kt-bg-dark" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        @break
                                        @endcase
                                        @case(1)
                                        <div class="progress-bar kt-bg-danger" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                        @break
                                        @endcase
                                        @case(2)
                                        <div class="progress-bar kt-bg-warning" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                        @break
                                        @endcase
                                        @case(3)
                                        <div class="progress-bar kt-bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                        @break
                                        @endcase
                                    @endswitch
                                </div>
                                <div class="kt-widget__stats">
                                    @switch($prospect->status)
                                        @case(0)
                                        25 %
                                        @break
                                        @endcase
                                        @case(1)
                                        50 %
                                        @break
                                        @endcase
                                        @case(2)
                                        75 %
                                        @break
                                        @endcase
                                        @case(3)
                                        100 %
                                    @endswitch
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-map"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Localisation du prospect
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-calendar"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Prochain rendez-vous
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="modal" data-target="#addRdv">
                                <i class="flaticon2-add-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div id="loadRdv"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addRdv" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Nouveau Rendez-vous</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form id="formAddRdv" action="/prospect/api/{{ $prospect->id }}/rdv" class="kt-form" method="POST">
                    <div class="modal-body">
                        @include("layout.include.alert")
                        <div class="form-group">
                            <label for="title">Titre <span class="required">*</span> </label>
                            <input type="text" class="form-control form-control-lg" name="title" placeholder="Intituler du rendez-vous...">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="start">Début <span class="required">*</span> </label>
                                <div class="input-group date">
                                    <input type="text" class="form-control datetime" name="start" id="kt_datetimepicker_8" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar glyphicon-th"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="end">Fin <span class="required">*</span> </label>
                                <div class="input-group date">
                                    <input type="text" class="form-control datetime" name="end" id="kt_datetimepicker_8" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar glyphicon-th"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" style="height: 325px;"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Nouveau rendez-vous</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="//maps.google.com/maps/api/js?key=AIzaSyBJPIZ4AUHlkaOEvOZTGrrI9-qu2XgYUsM"></script>
    <script src="https://raw.githubusercontent.com/HPNeo/gmaps/master/gmaps.js"></script>
    <script src="{{ asset('js/prospect/map.js') }}"></script>
    <script src="{{ asset('js/prospect/show.js') }}"></script>
@endsection
