<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env("APP_NAME") }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <style type="text/css">
        #mailable {
            position: relative;
            width: 494px;
            height: auto;
            display: flex;
        }
        header {
            position: absolute;
            width: 100%;
            height: 46px;
            background: #41AFFF;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        }

        .title{
            width: 100%;
            text-align: center;
            font-family: Roboto, serif;
            font-weight: bold;
            font-size: 18px;
            color: #fff;
            justify-content: center;
            display: flex;
            height: 46px;
            align-items: center;
        }
        .container {
            background-color: #f1ecf1;
            padding: 60px 10px;
            width: 100%;
        }

        p {
            font-family: Roboto, serif;
            color: black;
            font-size: 14px;
        }

        p.titleGuard {
            font-family: Roboto, serif;
            font-weight: bold;
            font-size: 18px;
            color: #41AFFF;
        }

        p.code {
            color: #41AFFF;
            font-weight: bold;
            text-align: center;
            font-size: 36px;
        }

        .grid {
            background-color: #E7E3E3;
            padding: 15px;
        }

        .grid p {
            font-size: 12px;
            font-weight: 300;
        }
    </style>
</head>
<body id="mailable">
    <header>
        <span class="title">{{ env("APP_NAME") }}</span>
    </header>
    <div class="container">
        @yield("content")
    </div>
</body>
</html>
