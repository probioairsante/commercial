@component('mail::message')
# Nouveau rendez-vous prospect

Vous avez insérer un nouveau rendez-vous dans l'interface de commercialisation de Resabilletcse<br>
Voici un récapitulatif:

    ## Prospect
    <table>
        <tbody>
            <tr>
                <td>Identité</td>
                <td> {{ $prospect->societe }} - {{ $prospect->name }}</td>
            </tr>
            <tr>
                <td>Adresse</td>
                <td>
                    {{ $prospect->adresse }}<br>
                    {{ $prospect->code_postal }} {{ $prospect->ville }}
                </td>
            </tr>
            <tr>
                <td>Coordonnées</td>
                <td>
                    <strong><i class="material-icons">email</i></strong>: {{ $prospect->email }}
                    @if(!empty($prospect->tel_fixe))
                        <strong><i class="material-icons">settings_phone</i></strong>: {{ $prospect->tel_fixe }}
                    @endif
                        @if(!empty($prospect->tel_portable))
                            <strong><i class="material-icons">phone_android</i></strong>: {{ $prospect->tel_portable }}
                        @endif
                </td>
            </tr>
        </tbody>
    </table>

    ## Rendez-vous

    <table>
        <thead>
            <tr>
                <th>Date</th>
                <th>Heure</th>
                <th>Titre</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    @if($event->start->format('d-m') == $event->end->format('d-m'))
                        {{ $event->start->format('d/m') }}
                    @else
                        du {{ $event->start->format('d/m') }} au {{ $event->end->format('d/m') }}
                    @endif
                </td>
                <td>{{ $event->start->format("H:i") }} - {{ $event->end->format("H:i") }}</td>
                <td>
                    <strong>{{ $event->title }}</strong><br>
                    @if($event->description != null)
                        <h6><i>{{ $event->description }}</i></h6>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>

@component('mail::button', ['url' => route('Prospect.show', $prospect->id)])
Voir la fiche du prospect
@endcomponent

{{ config('app.name') }}
@endcomponent
