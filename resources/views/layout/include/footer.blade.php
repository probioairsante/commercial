<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
    <div class="kt-footer__bottom">
        <div class="kt-container ">
            <div class="kt-footer__wrapper">
                <div class="kt-footer__logo">
                    <a href="{{ route('home') }}">
                        <img alt="Logo" src="/storage/logo_long.png" width="120">
                    </a>
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;
                        <a href="{{ env("APP_URL") }}" target="_blank">Resabilletcse</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
