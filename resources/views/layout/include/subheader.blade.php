<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">{{ env("APP_NAME") }}</h3>
            @foreach($pages as $page)
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">{{ $page['name'] }}</span>
            @endforeach

        </div>
        @if($back == true)
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a id="btnBack" href="" class="btn kt-subheader__btn-primary">
                        <i class="flaticon2-left-arrow"></i> Retour
                    </a>
                </div>
            </div>
        @endif
    </div>
</div>
