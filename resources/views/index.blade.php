@extends("layout.app")

@section("subheader")
    @include("layout.include.subheader", [
        "pages" => [
            [
                "name" => "Tableau de Bord",
                "link" => route('home')
            ]
        ],
        "back" => false
    ])
@endsection

@section("content")
    <!--<div class="alert alert-info  fade show" role="alert">
        <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
        <div class="alert-text">
            Pour acceder à la billetterie, veuillez utiliser les identifiants suivant:<br>
            <strong>Email:</strong> user0@billet.com<br>
            <strong>Mot de passe:</strong> user0
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </div>-->
    <div id="rdvPrevious"></div>
    <div class="row">
        <div class="col-md-8">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__body" id="map">

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Vos rendez-vous
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" id="loadRdv">

                </div>
            </div>
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Enregistrement
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="btn-group">
                        <button id="btn-start-recording" data-toggle="tooltip" title="Enregistrer" class="btn btn-icon btn-warning">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M12,16 C14.209139,16 16,14.209139 16,12 C16,9.790861 14.209139,8 12,8 C9.790861,8 8,9.790861 8,12 C8,14.209139 9.790861,16 12,16 Z M12,20 C7.581722,20 4,16.418278 4,12 C4,7.581722 7.581722,4 12,4 C16.418278,4 20,7.581722 20,12 C20,16.418278 16.418278,20 12,20 Z" fill="#000000" fill-rule="nonzero"/>
                                </g>
                            </svg>
                        </button>
                        <button id="btn-pause-recording" data-toggle="tooltip" title="Pause"  class="btn btn-icon btn-warning" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M8,6 L10,6 C10.5522847,6 11,6.44771525 11,7 L11,17 C11,17.5522847 10.5522847,18 10,18 L8,18 C7.44771525,18 7,17.5522847 7,17 L7,7 C7,6.44771525 7.44771525,6 8,6 Z M14,6 L16,6 C16.5522847,6 17,6.44771525 17,7 L17,17 C17,17.5522847 16.5522847,18 16,18 L14,18 C13.4477153,18 13,17.5522847 13,17 L13,7 C13,6.44771525 13.4477153,6 14,6 Z" fill="#000000"/>
                                </g>
                            </svg>
                        </button>
                        <button id="btn-stop-recording" data-toggle="tooltip" title="Stop"  class="btn btn-icon btn-warning" disabled>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M7.62302337,5.30262097 C8.08508802,5.000107 8.70490146,5.12944838 9.00741543,5.59151303 C9.3099294,6.05357769 9.18058801,6.67339112 8.71852336,6.97590509 C7.03468892,8.07831239 6,9.95030239 6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,9.99549229 17.0108275,8.15969002 15.3875704,7.04698597 C14.9320347,6.73472706 14.8158858,6.11230651 15.1281448,5.65677076 C15.4404037,5.20123501 16.0628242,5.08508618 16.51836,5.39734508 C18.6800181,6.87911023 20,9.32886071 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 C4,9.26852332 5.38056879,6.77075716 7.62302337,5.30262097 Z" fill="#000000" fill-rule="nonzero"/>
                                    <rect fill="#000000" opacity="0.3" x="11" y="3" width="2" height="10" rx="1"/>
                                </g>
                            </svg>
                        </button>
                    </div>
                    <br>
                    <ol id="recordingsList"></ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="//maps.google.com/maps/api/js?key=AIzaSyBJPIZ4AUHlkaOEvOZTGrrI9-qu2XgYUsM" type="text/javascript"></script>
    <script src="https://raw.githubusercontent.com/HPNeo/gmaps/master/gmaps.js" type="text/javascript"></script>
    <script src="https://cdn.rawgit.com/mattdiamond/Recorderjs/08e7abd9/dist/recorder.js"></script>
    <script src="{{ asset('js/map.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/index.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/audio.js') }}" type="text/javascript"></script>
@endsection
