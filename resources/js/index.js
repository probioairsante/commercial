import * as $ from "jquery";

function previousRdv() {
    let div = document.querySelector('#rdvPrevious')

    $.ajax({
        url: '/prospect/api/previousRdv',
        statusCode: {
            200: function (data) {
                div.innerHTML = data.data
            },
            500: function (jqxhr) {
                console.log(jqxhr)
            }
        }
    })
}

function loadRdv() {
    let div = document.querySelector('#loadRdv')

    $.ajax({
        url: '/prospect/api/loadRdv',
        statusCode: {
            200: function (data) {
                div.innerHTML = data.data.content
            },
            500: function (jqxhr) {
                console.log(jqxhr)
            }
        }
    })
}

previousRdv()
loadRdv()
