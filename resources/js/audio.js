let audio = document.querySelector('audio')
URL = window.URL || window.webkitURL;

let gumStream;
let rec;
let input

let AudioContext = window.AudioContext || window.webkitAudioContext
let audioContext = new AudioContext

let recordButton = document.getElementById('btn-start-recording')
let stopButton = document.getElementById('btn-stop-recording')
let pauseButton = document.getElementById('btn-pause-recording')

recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);
pauseButton.addEventListener("click", pauseRecording);

function startRecording() {
    let constraints = {
        audio: true,
        video: false
    }

    recordButton.disabled = true
    stopButton.disabled = false
    pauseButton.disabled = false

    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
        console.log("Success", "GetUserMedia Success, Stream Created, initializing recorder.js...")

        gumStream = stream

        input = audioContext.createMediaStreamSource(stream)

        rec = new Recorder(input, {
            numChannels: 1
        })

        rec.record()
        console.log("Log", "Record Started")
    }).catch((err) => {
        console.error("Erreur Stream Recording", err)
        recordButton.disabled = false
        stopButton.disabled = true
        pauseButton.disabled = true
    })
}

function pauseRecording() {
    console.log("pauseButton clicked rec.recording=", rec.recording);
    if (rec.recording) {
        //pause
        rec.stop();
        pauseButton.innerHTML = "Resume";
    } else {
        //resume
        rec.record()
        pauseButton.innerHTML = "Pause";
    }
}

function stopRecording() {
    console.log("stopButton clicked");
    //disable the stop button, enable the record too allow for new recordings
    stopButton.disabled = true;
    recordButton.disabled = false;
    pauseButton.disabled = true;
    //reset button just in case the recording is stopped while paused
    pauseButton.innerHTML = "Pause";
    //tell the recorder to stop the recording
    rec.stop(); //stop microphone access
    gumStream.getAudioTracks()[0].stop();
    //create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(createDownloadLink);
}

function createDownloadLink(blob) {
    let url = URL.createObjectURL(blob);
    let au = document.createElement('audio');
    let li = document.createElement('li');
    let link = document.createElement('a');
    //add controls to the <audio> element
    au.controls = true;
    au.src = url;
    //link the a element to the blob
    link.href = url;
    link.download = new Date().toISOString() + '.wav';
    link.innerHTML = link.download;
    //add the new audio and a elements to the li element
    li.appendChild(au);
    li.appendChild(link);
    //add the li element to the ordered list
    recordingsList.appendChild(li);
}

