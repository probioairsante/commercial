import * as $ from "jquery";
const $map = document.querySelector('#map')
const GMaps = require('gmaps/gmaps')

function genMap() {
    let map = new GMaps({
        div: $map,
        zoom: 7,
        lat: 46.59490470370535,
        lng: 0.31860761749688216
    })

    $.get('/prospect/api/loadRdv')
        .done((data) => {
            Array.from(data.data.info).forEach((item) => {
                let info = item
                $.ajax({
                    url: 'https://api.opencagedata.com/geocode/v1/json?',
                    data: {
                        q: item.adresse+','+item.code_postal+' '+item.ville+' FR',
                        key: "904128a131ac44109d595c310bf7cadc",
                        pretty: 1,
                        no_annotations: 1
                    },
                    success: function (data) {
                        console.log(data)
                        console.log(info)
                        addMarker(map, data.results[0].geometry.lat, data.results[0].geometry.lng, info.prospect.societe+' - '+info.prospect.name, info)
                    }
                })
            })
        })
}

function addMarker(map, lat, lng, title, info) {
    map.addMarker({
        lat: lat,
        lng: lng,
        title: title,
        infoWindow:
            `<div id="content">
    <div id="siteNotice"></div>
    <h1 id="firstHeading" class="firstHeading">${title}</h1>
    <div id="bodyContent">
        <strong>Email </strong>: ${info.prospect.email}<br>
        <strong>Téléphone Fixe </strong>: ${info.prospect.tel_fixe}<br>
        <strong>Téléphone Portable </strong>: ${info.prospect.tel_portable}<br>
    </div>    
</div>`
    })
}

genMap()
