import '../demo2/tools/webpack/vendors/global';
import '../demo2/tools/webpack/scripts';
import Echo from 'laravel-echo'


require('./bootstrap');
require('./core')

let echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
})

echo.channel('auth')
    .listen('ConnectionInfused', function (event) {
        console.log(event)
    })

$(".selectpicker").selectpicker()


