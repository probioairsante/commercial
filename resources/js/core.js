import * as $ from "jquery";

class Core {
    constructor() {

    }

    errorMsg(form, type, msg) {
        let alert = `<div class="alert alert-bold alert-solid-${type} alert-dismissible" role="alert">
                        <div class="alert-text">${msg}</div>
                        <div class="alert-close">
                            <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>    
                        </div>
                    </div>`

        form.find('.alert').html(alert)
    }

    readNotification() {
        let notifications = document.querySelectorAll('.kt-notification__item')

        Array.from(notifications).forEach((item) => {
            item.addEventListener('click', function (e) {
                e.preventDefault()
                let id = item.dataset.id;

                if (item.dataset.href !== null) {
                    window.location.href=item.dataset.href
                }else{
                    $.ajax({
                        url: '/api/account/notification/' + id + '/read',
                        statusCode: {
                            200: function (data) {
                                item.classList.add('kt-notification__item--read')
                            },
                            422: function (data) {
                                toastr.error("Erreur Serveur")
                            }
                        }
                    })
                }
            })
        })
    }


}

const Init = function () {
    let core = new Core()
    core.readNotification()
}

$("#btnBack").on('click', () => {
    history.back();
})

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Init()

export default Core
