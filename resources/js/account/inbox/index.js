import * as $ from "jquery";

class Inbox {
    constructor() {
    }

    readCookie(name) {
        let nameEQ = name + "=";
        let ca = document.cookie.split(';');
        for(let i=0;i < ca.length;i++) {
            let c = ca[i];
            while (c.charAt(0)===' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    addingClassInbox() {
        let body = document.querySelector('.kt-page--loading-enabled')
        body.classList.add('kt-inbox__aside--left')
    }

    viewType() {
        let element = document.querySelectorAll('.kt-nav__link')
        console.log()
        Array.from(element).forEach((item) => {
            item.addEventListener('click', function (event) {
                let viewbox = $('.kt-inbox__items');
                $.ajax({
                    url: '/account/inbox/getList/'+item.dataset.type,
                    statusCode: {
                        200: function (data) {
                            viewbox.html(data.data)
                        },
                        500: function (jqxhr) {
                            console.error(jqxhr)
                        }
                    }
                })

            })

        })
    }


}

const init = function () {
    let inbox = new Inbox()
    inbox.addingClassInbox()
    inbox.viewType()
}

init()
