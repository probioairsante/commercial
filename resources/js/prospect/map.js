import * as $ from "jquery";
const GMaps = require('gmaps/gmaps')

let $map = document.querySelector('#map')
let prospect_id = document.querySelector('#prospect').dataset.id
let address = document.querySelector('#prospect').dataset.address
let societe = document.querySelector('#prospect').dataset.societe
let name = document.querySelector('#prospect').dataset.name
let fixe = document.querySelector('#prospect').dataset.fixe
let portable = document.querySelector('#prospect').dataset.portable
let email = document.querySelector('#prospect').dataset.email


function covermap() {
    $.ajax({
        url: 'https://api.opencagedata.com/geocode/v1/json?',
        data: {
            q: address,
            key: "904128a131ac44109d595c310bf7cadc",
            pretty: 1,
            no_annotations: 1
        },
        success: function (data) {
            let map = new GMaps({
                div: $map,
                zoom: 15,
                lat: data.results[0].geometry.lat,
                lng: data.results[0].geometry.lng,
            })

            map.addMarker({
                lat: data.results[0].geometry.lat,
                lng: data.results[0].geometry.lng,
                title: societe+' '+name,
                infoWindow: {
                    content:
`<div id="content">
    <div id="siteNotice"></div>
    <h1 id="firstHeading" class="firstHeading">${societe} - ${name}</h1>
    <div id="bodyContent">
        <strong>Email </strong>: ${email}<br>
        <strong>Téléphone Fixe </strong>: ${fixe}<br>
        <strong>Téléphone Portable </strong>: ${portable}<br>
    </div>    
</div>`

                }
            })
        }
    })

}

function loadCoordinate() {

}

covermap()

