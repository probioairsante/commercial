import * as $ from "jquery";
const moment = require('moment')


class ProspectShow {
    constructor() {
        this.prospect_id = document.querySelector('#prospect').dataset.id
    }

    changeStatus() {
        let links = document.querySelectorAll('.link-status')
        let prospect_id = this.prospect_id

        Array.from(links).forEach((item) => {
            item.addEventListener('click', function (event) {
                let status = item.dataset.status

                $.ajax({
                    url: '/api/prospect/'+prospect_id+'/changeStatus/'+status,
                    statusCode: {
                        200: function (data) {
                            toastr.success("Le status à été changer");
                            window.setTimeout(function () {
                                location.reload()
                            }, 1200)
                        },
                        500: function (jqxhr) {
                            toastr.error("Erreur lors du changement de status")
                        }
                    }
                })
            })
        })
    }

    loadCalendar() {
        let rdvEl = document.querySelector('#loadRdv')

        KTApp.block(rdvEl, {
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            size: 'lg',
            message: 'Chargement des rendez-vous...'
        })

        $.get('/prospect/api/'+this.prospect_id+'/loadRdv')
            .done((data) => {
                KTApp.unblock(rdvEl)
                rdvEl.innerHTML = data.data
            })
    }

    postRdv() {
        let form = $("#formAddRdv");
        let btn = form.find('.btn')

        form.on('submit', function (e) {
            e.preventDefault()

            KTApp.progress(btn)

            $.ajax({
                url: form.attr('action'),
                method: "POST",
                data: form.serializeArray(),
                statusCode: {
                    200: function (data) {
                        KTApp.unprogress(btn)
                        KTUtil.animateClass($(".kt-timeline-v3__items").prepend(data.data.content), 'flipInX animated')
                        console.log(data)
                    },
                    203: function (data) {
                        KTApp.unprogress(btn)
                        let alertEl = document.querySelector('#alertFormError')
                        alertEl.style.display = 'block'
                        let errorsEl = $('#errors')
                        Array.from(data.data.errors).forEach((item) => {
                            errorsEl.html(`<li>${item}</li>`)
                        })
                    },
                    500: function (jqxhr) {
                        KTApp.unprogress(btn)
                        console.log(jqxhr)
                    }
                }
            })
        })
    }

}


(($) => {
    let prospect = new ProspectShow()
    prospect.changeStatus()
    prospect.loadCalendar()
    prospect.postRdv()

    $(".datetime").datetimepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy hh:ii',
        language: moment.locale('fr')
    });

    $("#description").summernote({
        height: 150
    })

})(jQuery)
