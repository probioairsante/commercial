import * as $ from "jquery";
import errorMsg from '../core'

class Login {
    constructor() {
        this.login = document.querySelector('#kt_login')
        this.form = document.querySelector('#kt_login_form')
    }

    signIn() {
        let form = $("#kt_login_form")
        let error = new errorMsg()
        this.form.addEventListener('submit', (e) => {
            e.preventDefault()
            let btn = document.querySelector('#kt_login_signin_submit')
            let data = form.serializeArray()

            KTApp.progress(btn)

            $.ajax({
                url: form.attr('action'),
                method: "POST",
                datatype: 'json',
                data: data,
                statusCode: {
                    200: function (data) {
                        KTApp.unprogress()
                        console.log(data)
                       window.location.href='/'
                    },
                    422: function (data) {
                        KTApp.unprogress()
                        let responses = data.responseJSON.errors
                        toastr.error(responses.email, "Erreur Identifiant")
                    },
                    419: function () {
                        KTApp.unprogress()
                        error.errorMsg(this.login, 'danger', "Token Invalide")
                    },
                    404: function () {
                        KTApp.unprogress()
                        error.errorMsg(this.login, 'danger', "Route non définie")
                    }
                }
            })
        })
    }
}

const init = function() {
    let login = new Login()
    login.signIn()
}

init();
